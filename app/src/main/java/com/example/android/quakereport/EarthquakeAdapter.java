package com.example.android.quakereport;

import android.app.Activity;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by chiaweihuMBP on 2017/04/22.
 */

public class EarthquakeAdapter extends ArrayAdapter<Earthquake> {

    private static final String LOCATION_SEPARATOR = " of ";

    public EarthquakeAdapter(Activity context, ArrayList<Earthquake> earthquakes) {
        super(context, 0, earthquakes);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }

        // get the Earthquake object located at this position in the list
        Earthquake earthquake = getItem(position);

        // set magnitude
        TextView magnitudeTextView = (TextView) listItemView.findViewById(R.id.text_magnitude);
        magnitudeTextView.setText(formatMagnitude(earthquake.getMagnitude()));
        // Set the proper background color on the magnitude circle
        // Fetch the background from the TextView, which is a GradientDrawable
        GradientDrawable magnitudeCircle = (GradientDrawable) magnitudeTextView.getBackground();
        // Get the appropriate background color based on the current earthquake magnitude
        int magnitudeColor = getMagnitudeColor(earthquake.getMagnitude());
        // Set the color on the magnitude circle
        magnitudeCircle.setColor(magnitudeColor);

        // get raw location string
        String rawLocation = earthquake.getLocation();
        String locationOffset;
        String primaryLocation;
        // extract distance and location
        if (rawLocation.contains(LOCATION_SEPARATOR)) {
            String[] parts = rawLocation.split(LOCATION_SEPARATOR);
            locationOffset = parts[0] + LOCATION_SEPARATOR;
            primaryLocation = parts[1];
        } else {
            locationOffset = getContext().getString(R.string.near_the);
            primaryLocation = rawLocation;
        }

        // set distance offset
        TextView distanceTextView = (TextView) listItemView.findViewById(R.id.text_distance);
        distanceTextView.setText(locationOffset);

        // set location
        TextView locationTextView = (TextView) listItemView.findViewById(R.id.text_location);
        locationTextView.setText(primaryLocation);

        // get raw occur datetime
        Date dateObject = new Date(earthquake.getOccurDateInMiniSeconds());

        // set occur date
        TextView occurDateTextView = (TextView) listItemView.findViewById(R.id.text_occur_date);
        // Format the date string (i.e. "Mar 3, 1984")
        String formattedDate = formatDate(dateObject);
        occurDateTextView.setText(formattedDate);

        // set occur time
        TextView occurTimeTextView = (TextView) listItemView.findViewById(R.id.text_occur_time);
        // format time time string
        String formattedTime = formatTime(dateObject);
        occurTimeTextView.setText(formattedTime);

        // return super.getView(position, convertView, parent);
        return listItemView;
    }

    /**
     * Return the formatted date string (i.e. "Mar 3, 1984") from a Date object.
     */
    private String formatDate(Date dateObject) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("LLL dd, yyyy");
        return dateFormat.format(dateObject);
    }

    /**
     * Return the formatted date string (i.e. "4:30 PM") from a Date object.
     */
    private String formatTime(Date dateObject) {
        SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");
        return timeFormat.format(dateObject);
    }

    private String formatMagnitude(double mag) {

        DecimalFormat formatter = new DecimalFormat("0.0");
        return formatter.format(mag);
    }

    private int getMagnitudeColor(double magnitude) {
        int colorResourceId;

        int magnitudeFloor = (int) Math.floor(magnitude);
        switch (magnitudeFloor) {
            case 0:
            case 1:
                colorResourceId = R.color.magnitude1;
                break;
            case 2:
                colorResourceId = R.color.magnitude2;
                break;
            case 3:
                colorResourceId = R.color.magnitude3;
                break;
            case 4:
                colorResourceId = R.color.magnitude4;
                break;
            case 5:
                colorResourceId = R.color.magnitude5;
                break;
            case 6:
                colorResourceId = R.color.magnitude6;
                break;
            case 7:
                colorResourceId = R.color.magnitude7;
                break;
            case 8:
                colorResourceId = R.color.magnitude8;
                break;
            case 9:
                colorResourceId = R.color.magnitude9;
                break;
            case 10:
            default:
                colorResourceId = R.color.magnitude10plus;
                break;
        }
        // call ContextCompat getColor() to convert the color resource ID
        // into an actual integer color value
        return ContextCompat.getColor(getContext(), colorResourceId);
    }
}
