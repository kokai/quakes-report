package com.example.android.quakereport;

import android.content.AsyncTaskLoader;
import android.content.Context;

import java.util.List;

/**
 * Created by chiaweihuMBP on 2017/04/26.
 */

public class EarthquakeLoader extends AsyncTaskLoader<List<Earthquake>> {

    // tag for log messages
    private static final String LOG_TAG = EarthquakeLoader.class.getName();

    // query url
    private String mUrl;

    /**
     * Constructs a new {@link EarthquakeLoader}.
     *
     * @param context of the activity
     * @param url to load data from
     */
    public EarthquakeLoader(Context context, String url) {
        super(context);

        mUrl = url;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    /**
     * This is on a background thread.
     */
    @Override
    public List<Earthquake> loadInBackground() {
        if (mUrl == null) return null;

        // Perform the HTTP request for earthquake data and process the response.
        List<Earthquake> earthquakes =  QueryUtils.fetchEarthquakesData(mUrl);
        return earthquakes;
    }
}
