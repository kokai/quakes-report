package com.example.android.quakereport;

/**
 * Created by chiaweihuMBP on 2017/04/22.
 */

public class Earthquake {

    private double mMagnitude;
    private String mLocation;
    private long mOccurDateInSeconds;
    /** Website URL of the earthquake */
    private String mInfoUrl;

    /**
     * Constructs a new {@link Earthquake} object.
     *
     * @param mag is the magnitude (size) of the earthquake
     * @param loc is the location where the earthquake happened
     * @param date is the time in milliseconds (from the Epoch) when the
     *                           earthquake happened
     * @param url is the website URL to find more details about the earthquake
     */
    public Earthquake(double mag, String loc, long date, String url) {
        mMagnitude = mag;
        mLocation = loc;
        mOccurDateInSeconds = date;
        mInfoUrl = url;
    }

    public double getMagnitude() {
        return mMagnitude;
    }

    public String getLocation() {
        return mLocation;
    }

    public long getOccurDateInMiniSeconds() {
        return mOccurDateInSeconds;
    }

    /**
     * Returns the website URL to find more information about the earthquake.
     */
    public String getInfoUrl() {
        return mInfoUrl;
    }
}
